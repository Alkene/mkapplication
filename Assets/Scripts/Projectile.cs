﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour 
{
    public float m_Speed;

    public float m_LifeTime;

    public string m_EnvironmentTag = "Environment";

    private void OnTriggerEnter(Collider _other)
    {
        if (_other.CompareTag(m_EnvironmentTag))
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        Destroy(gameObject, m_LifeTime);
    }

    private void FixedUpdate()
    {
        transform.position += transform.forward * m_Speed * Time.fixedDeltaTime;
    }
}
