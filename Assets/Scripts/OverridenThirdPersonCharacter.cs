﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class OverridenThirdPersonCharacter : ThirdPersonCharacter 
{
    public GameObject m_LaserPrefab;
    public Transform m_LaserOrigin;

    public float m_FireDelay;

    private float m_FireTimer = 0.0f;

    public void AttemptToFire()
    {
        if (m_FireTimer < Time.time)
        {
            GameObject go = Instantiate(m_LaserPrefab, m_LaserOrigin.position, m_LaserOrigin.rotation);
            m_FireTimer = Time.time + m_FireDelay;
        }
    }
}
