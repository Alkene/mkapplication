﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingCamera : MonoBehaviour 
{
    public Transform m_TrackedTransform;
    public Vector3 m_CameraRuntimeLookOffset;
    public Vector3 m_LookOffset;

    public Vector3 m_TrackOffset;

    public float m_CameraRuntimeLookAmount;

    private void OnValidate()
    {
        LateUpdate();
    }

    private void LateUpdate()
    {
        transform.position = m_TrackedTransform.position + m_TrackOffset + m_CameraRuntimeLookOffset * m_CameraRuntimeLookAmount;
        transform.LookAt(m_TrackedTransform.position + m_LookOffset + m_CameraRuntimeLookOffset * m_CameraRuntimeLookAmount, Vector3.up);
    }
}
