﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(OverridenThirdPersonCharacter))]
public class OverridenThirdPersonUserControl : ThirdPersonUserControl 
{
    public Transform m_EthanPivot;

    public TrackingCamera m_MainCamera;

    private Vector2 m_LookVector;
    private Vector2 m_MoveVector;

    protected override void Start()
    {
        // get the transform of the main camera
        if (Camera.main != null)
        {
            m_Cam = Camera.main.transform;
        }
        else
        {
            Debug.LogWarning(
                "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
            // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
        }

        // get the third person character ( this should never be null due to require component )
        m_Character = GetComponent<OverridenThirdPersonCharacter>();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        // Turn
        m_LookVector = new Vector2(CrossPlatformInputManager.GetAxis("Fire Horizontal"), CrossPlatformInputManager.GetAxis("Fire Vertical"));
        m_MoveVector = new Vector2(CrossPlatformInputManager.GetAxis("Horizontal"), CrossPlatformInputManager.GetAxis("Vertical"));
        if (m_LookVector.sqrMagnitude > 1)
        {
            m_LookVector.Normalize();
        }

        if (IsLooking())
        {
            m_EthanPivot.localEulerAngles = new Vector3(0.0f, Mathf.Atan2(-m_LookVector.y, m_LookVector.x) * Mathf.Rad2Deg + 90.0f - m_Character.transform.localEulerAngles.y, 0.0f);
            ((OverridenThirdPersonCharacter)m_Character).AttemptToFire();

            m_MainCamera.m_CameraRuntimeLookOffset = Vector3.Lerp(m_MainCamera.m_CameraRuntimeLookOffset, m_EthanPivot.forward, Time.deltaTime * 10);
        }
        else if (IsMoving()) 
        {
            m_EthanPivot.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
            m_MainCamera.m_CameraRuntimeLookOffset = Vector3.Lerp(m_MainCamera.m_CameraRuntimeLookOffset, Vector3.zero, Time.deltaTime * 10);
        }
        else 
        {
            m_MainCamera.m_CameraRuntimeLookOffset = Vector3.Lerp(m_MainCamera.m_CameraRuntimeLookOffset, Vector3.zero, Time.deltaTime * 10);
        }
    }

    private bool IsLooking()
    {
       return (m_LookVector.sqrMagnitude > float.Epsilon); 
    }

    private bool IsMoving()
    {
        return (m_MoveVector.sqrMagnitude > float.Epsilon);
    }
}
